# \<button-back\>

[![Pipeline status][pipeline-badge]][pipeline]
[![Published on NPM][npm-badge]][npm]
[![MIT License][license-badge]][license]

Element to go to the previous page

This element doesn't work like `window.history.back()` but compute a link from the given URL to go to the previous folder (or slash in the URL)

- [Install](#install)
	- [Install with Yarn](#install-with-yarn)
	- [Install with NPM](#install-with-npm)
		- [Simple install](#simple-install)
		- [Manual install](#manual-install)
	- [Install with Bower](#install-with-bower)
- [Usage](#usage)
- [License](#license)


## Install

### Install with Yarn

Using [Yarn][yarn]

```sh
$ yarn add button-back
```


### Install with NPM

#### Simple install

```sh
$ npm install button-back --save
```


#### Manual install

First, make sure you have [Bower][bower] and [Polymer CLI][polymer-cli] installed

```sh
$ npm install button-back --no-optional --save
```


### Install with Bower

First, make sure you have [Bower][bower] and [Polymer CLI][polymer-cli] installed

```sh
$ bower install button-back --save
$ npm run-script analyze # if you need to watch the documentation
```


## Usage

`<button-back>` need `route` provided by `<app-location>` or `<app-route>` to be notified when the route is modified

<!--
```html
<custom-element-demo>
	<template>
		<link rel="import" href="bower_components/app-route/app-location.html" />
		<link rel="import" href="button-back.html" />
		<next-code-block></next-code-block>
	</template>
</custom-element-demo>
```
-->

```html
<app-location
	route="{{route}}"
	use-hash-as-path
></app-location>
<button-back
	route="{{route}}"
	use-hash-as-path
></button-back>
```


## License

[MIT Licence][license]



[pipeline-badge]: https://gitlab.com/pinage404/button-back/badges/master/pipeline.svg
[pipeline]:   https://gitlab.com/pinage404/button-back/commits/master

[npm-badge]: https://img.shields.io/npm/v/button-back.svg
[npm]:   https://www.npmjs.com/package/button-back

[license-badge]: https://img.shields.io/badge/license-MIT-green.svg
[license]: https://gitlab.com/pinage404/button-back/blob/master/LICENSE

[yarn]: https://yarnpkg.com

[bower]:       https://bower.io/#install-bower
[polymer-cli]: https://www.npmjs.com/package/polymer-cli
